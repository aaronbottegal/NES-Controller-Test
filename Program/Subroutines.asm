WaitForNMI:
  LDA Frame
.Loop:
  CMP Frame
  BEQ .Loop
  RTS

DecompressToPPU:
  LDA RLEPointersLow,X
  STA <DecompressDataPointer
  LDA RLEPointersHigh,X
  STA <DecompressDataPointer+1
DecompressToPPUPreset:
  LDY #$00
  LDA [DecompressDataPointer],Y
  STA <CompressedDataFlag
  INY
.DecompressMainLoop:
  LDA [DecompressDataPointer],Y
  CMP <CompressedDataFlag
  BEQ .CompressedData
  STA $2007
.DecompressMainLoopReEnter:
  INY  
  BNE .DecompressMainLoop
  INC <DecompressDataPointer+1
  JMP .DecompressMainLoop
.ReturnDecompressToPPU:
  RTS
.CompressedData:
  INY
  BNE .NoPageCross1
  INC <DecompressDataPointer+1
.NoPageCross1:
  LDA [DecompressDataPointer],Y
  BEQ .ReturnDecompressToPPU
  TAX
  INY 
  BNE .NoPageCross2
  INC <DecompressDataPointer+1
.NoPageCross2:
  LDA [DecompressDataPointer],Y
.ToPPULoop:
  STA $2007
  DEX
  BNE .ToPPULoop
  BEQ .DecompressMainLoopReEnter

ReadControllers:
  LDX #$01
  STX $4016
  STX ControllersLow
  STX ControllersLow+1 
  DEX
  LDA ControllersHigh
  STA ControllersHighOld
  LDA ControllersHigh+1
  STA ControllersHighOld+1
  STX $4016
  STX ControllersHigh
  STX ControllersHigh+1
  INX
.Loop:
  LDA $4016,X
  LSR A
  ROL ControllersLow,X  
  ROL ControllersHigh,X
  BCC .Loop
  LDA ControllersHighOld,X ;Get old buttons.
  EOR #$FF ;See which weren't pressed.
  AND ControllersHigh,X ;See which are newly pressed.
  STA ControllersPressed,X ;Store newly pressed buttons.
  DEX ;See if we're on player 1's controller now.
  BPL .Loop ;Do move, read, pressed again.
  RTS

SpriteAnimationToScreen:
  LDA CurrentStep
  AND #%00000011
  TAX ;X=Animation Index.
  ASL A ;*2
  ASL A ;*4
  ASL A ;*8
  STA AddAnimation ;Put add value to RAM to pick sprite.
  LDA #$07
  STA AnimationPutLoop
  LDY #$00
.Loop:
  LDA MegaManBase+1,Y ;YTAX:0123. Grab original value.
  CLC
  ADC AddAnimation
  STA SpritePage+1,Y ;Store to OAM RAM.
  LDA MegaManBase,Y ;Get Y.
  CLC
  ADC OffsetY
  ADC #MegaManYOffsetConstant
  STA SpritePage,Y ;Store Y.
  LDA MegaManBase+3,Y ;Get X.
  CLC
  ADC OffsetX
  ADC #MegaManXOffsetConstant
  ADC MegaManBaseXOffsets,X
  STA SpritePage+3,Y ;Store X.
  LDA MegaManBase+2,Y
  STA SpritePage+2,Y
  INY
  INY
  INY
  INY
  DEC AnimationPutLoop
  BPL .Loop
  LDA MegaManFace+1
  STA SpritePage+1,Y
  LDA MegaManFace+2
  STA SpritePage+2,Y
  LDA MegaManBaseFaceYOffsets,X
  CLC 
  ADC OffsetY
  ADC #MegaManYOffsetConstant
  STA SpritePage,Y
  LDA MegaManBaseFaceXOffsets,X
  CLC
  ADC OffsetX
  ADC #MegaManXOffsetConstant
  ADC MegaManBaseXOffsets,X
  STA SpritePage+3,Y
.ReturnSR
  RTS

WordsToBuffer:
  LDY #$00
.Loop:
  LDA WordsPage,X
  STA MessageBuffer,Y
  INX
  INY
  CPY #$20
  BCC .Loop
  RTS

UpdateButtonsBuffer:
  LDY #$00
  STY UBScratch ;Loop counter. Sadly, we must count up. :(
.MLoop:
  LDA PressIndex ;GetIndex currently at.
  CLC
  ADC UBScratch ;Put number of entry in there.
  LSR A ;Decide if shifting is needed.
  TAX ;X=Index of byte.
  LDA PressMap,X ;Get value.
  BCS .NoShiftNeeded
  LSR A
  LSR A
  LSR A
  LSR A
.NoShiftNeeded:
  AND #%00001111 ;Isolate index of text value needed.
  ASL A ;A=A*2
  STA UBMUL ;Save 2A
  ASL A ;4A
  ADC UBMUL ;2A+4A=6A.
  TAX ;Store tile index to X.
  LDA #$05
  STA UBTL ;Transfer Loop count.
.TLoop:
  LDA ButtonTexts,X
  STA ButtonNamesBuffer,Y
  INX
  INY
  DEC UBTL
  BPL .TLoop
  INC UBScratch ;Add 1 loop done.
  LDA UBScratch ;Load number.
  CMP #$03
  BNE .MLoop ;If not, go to main loop again.
  RTS

WaitToStartGame:
  LDA ControllersPressed 
  ORA ControllersPressed+1
  BNE .Start
  RTS
.Start:
  LDA #%00010000
  STA GameStatus
  LDX #$20
  JSR WordsToBuffer ;Put words to buffer for the "HUD" line.

  LDX #10
  LDA #$00
.LoopClearPBuffer:
  STA PutBuffer,X
  DEX
  BPL .LoopClearPBuffer
  STA PutsNext  ;Set up vars. Clear buffer, set next. Now we assign X's.
  LDA Frame ;Get frame.
  AND #%00000111 ;Get bottom 3 bits.
  STA PutXs ;Put index to start in 1.
  LDA Frame
  LSR A
  LSR A
  LSR A
  AND #%00000111 ;Get bottom 3 bits again.
  CLC
  ADC #$03 ;3 to shove the 7 bits to the last 10 locations in the array. AKA backwards.
  STA PutXs+1 ;Put X array.
  LDA Frame
  ROL A
  ROL A
  ROL A
  AND #%00000011 ;Get SBC size for pointer 2.
  BNE .MoreThanZeroMove
  ORA #$01
.MoreThanZeroMove:
  STA SecondSubtract ;Value to subtract each time.
  LDA #11 ;BNE loop value.
  STA PutsToDo
.MainRandLoop:
  LDX PutXs+1
  BMI .SkipSecond ;Is less than 0 in array? If so no modification in any way.
  LDA PutBuffer,X
  BNE .NoWrite2 ;Is flag already set? If so, skip this single itteration.
  INC PutBuffer,X ;Set byte flag.
  LDY PutsNext ;Get next index location.
  TXA
  ASL A
  TAX ;X=Index to data bytes needed.
  LDA ButtonCombos,X ;Store 2 data bytes.
  STA PressMap,Y
  LDA ButtonCombos+1,X
  STA PressMap+1,Y
  DEC PutsToDo ;See if we're done.
  BEQ .RandDone ;YES!
  INY
  INY
  STY PutsNext ;Store next put location to memory.
.NoWrite2:
  LDA PutXs+1 ;Change X index for next time.
  SEC
  SBC SecondSubtract
  STA PutXs+1
.SkipSecond:
  LDX PutXs
  LDA PutBuffer,X
  BNE .NoWrite1
  INC PutBuffer,X
  TXA
  ASL A
  TAX ;X=Index to data bytes needed.
  LDY PutsNext
  LDA ButtonCombos,X ;Store 2 data bytes.
  STA PressMap,Y
  LDA ButtonCombos+1,X
  STA PressMap+1,Y
  DEC PutsToDo ;See if we're done.
  BEQ .RandDone ;YES!
  INY
  INY
  STY PutsNext ;Store next put location to memory.
.NoWrite1:
  LDX PutXs
  INX
  CPX #$11
  BNE .NoFix1
  LDX #$00
.NoFix1:
  STX PutXs
  JMP .MainRandLoop
.RandDone:
  RTS ;Everything done, return to main engine.

MainGame:
  LDA PressIndex
  LSR A
  TAX ;Index of press to X.
  LDA PressMap,X
  BCS .NoShift
  LSR A
  LSR A
  LSR A
  LSR A
.NoShift:
  AND #$0F ;Get bottom 4 bits.
  CMP #$08 ;Button?
  BCS .GameFinished ;Nope, game is over at minimum.
  TAX ;Index of button needed to X
  LDA ControllersPressed ;Get buttons.
  ORA ControllersPressed+1
  CMP BitTestHL,X ;Test button needed.
  BNE .NoNextStep ;If no, nope.
  INC CurrentStep
  INC PressIndex
  LDA OffsetX
  CLC
  ADC #$04
  STA OffsetX
.NoNextStep:
  RTS
.GameFinished:
  LDA GameStatus
  AND #%00001000
  BNE .GameOverResetWait
  LDX #$40
  JSR WordsToBuffer
  LDA GameStatus
  ORA #%00001000 ;Set to game over.
  STA GameStatus
  RTS
.GameOverResetWait:
  LDA ControllersLow
  ORA ControllersLow+1
  BEQ .ResetGame ;If no controller in either port.
  LDA ControllersHigh
  ORA ControllersHigh+1
  AND #(StartButton+SelectButton)
  CMP #(StartButton+SelectButton)
  BEQ .ResetGame ;If start+select pressed on either port.
  RTS
.ResetGame:
  PLA
  PLA ;Pull RTS off the stack.
  JMP MainEngineSetup