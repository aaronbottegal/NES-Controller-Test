SpritePage=$0200;

PPUCtrl=$2000;
PPUMask=$2001;
PPUStatus=$2002;
PPUOamAddr=$2003;
PPUOamData=$2004;
PPUScroll=$2005;
PPUAddr=$2006;
PPUData=$2007;
PPUDma=$4014;

MegaManXOffsetConstant=$1C;
MegaManYOffsetConstant=$3F;

UpButton=$08;
DownButton=$04;
LeftButton=$02;
RightButton=$01;
AButton=$80;
BButton=$40;
SelectButton=$20;
StartButton=$10;