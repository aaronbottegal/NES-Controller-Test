  .inesprg 1   ;1x 16KB PRG code.
  .ineschr 1   ;1x  8KB CHR data.
  .inesmap 0   ;Mapper 0 = NROM, no bank swapping.
  .inesmir 1   ;Background mirroring: 1 for Vertical, 0 for Horizontal.

  .include "Program/Constants.asm"

  .rsset $0000
Zero: .rs 1
ZeroWrite: .rs 1
;RLERoutineVars
DecompressDataPointer: .rs 2
CompressedDataFlag: .rs 1


  .rsset $100
RAMPalette: .rs 32
MessageBuffer: .rs 32
ButtonNamesBuffer: .rs 18

  .rsset $300
OffsetX: .rs 1
OffsetY: .rs 1
AddAnimation:  .rs 1
CurrentStep: .rs 1
AnimationPutLoop: .rs 1

GameStatus: .rs 1

Frame: .rs 1
PPUCtrlRAM: .rs 1
PPUMaskRAM: .rs 1
ControllersHigh: .rs 2 ;Where data goes.
ControllersHighOld: .rs 2
ControllersPressed: .rs 2
ControllersLow: .rs 2 ;Used to see if controller is plugged in, is used.
SaveStack: .rs 1

PressIndex: .rs 1
PressMap: .rs 24
UBScratch: .rs 1
UBMUL: .rs 1
UBTL: .rs 1

PutBuffer: .rs 11 ;11 single byte flags to remember which combos have been put up.
PutXs: .rs 2 ;Depths of put buffers.
SecondSubtract: .rs 1 ;2nd buffer subtracts each time. This is the amount.
PutsToDo: .rs 1 ;Counts down how many 2x writes are left.
PutsNext: .rs 1;Next index to write.

PaletteRoll: .rs 1
PRC: .rs 1


  .bank 0
  .org $C000
  .include "Program/Data.asm"

  .include "Program/Subroutines.asm"

  .bank 1
  .org $E000
Reset:
  .include "Program/Startup.asm"
  LDY #$00
  STY PPUCtrlRAM
  STY PPUCtrl
  LDX #$1F
;Upload the pallet TO RAM. We upload it in VBlank.
.PaletteLoop:
  LDA Pallet,X
  STA RAMPalette,Y
  INY
  DEX
  BPL .PaletteLoop
  LDX #$20
  STX PPUAddr
  LDX #$00
  STX PPUAddr ;X also doubles as the pointer to the RLE screen needed.
  JSR DecompressToPPU
  LDA #%10100000
  STA PPUCtrlRAM
  STA PPUCtrl

  LDA #$21
  STA PaletteRoll ;Set pallet roll.

MainEngineSetup:
  LDA #$00
  STA PressIndex ;Reset button press index.
  STA GameStatus ;Reset game status to waiting.
  STA OffsetX ;Reset megaman's position.
  LDX #21 ;Button presses buffer size.
  LDA #$88
.ClearPresses:
  STA PressMap,X
  DEX
  BPL .ClearPresses
  LDA #$99
  STA PressMap+22
  STA PressMap+23

  LDX #$00
  JSR WordsToBuffer ;Put words to buffer.

  LDX #18 ;Number to loop.
  LDA #$2D ;Dash character.
.Loop:
  STA ButtonNamesBuffer,X
  DEX
  BPL .Loop ;Write dash's.

MainEngine:
  JSR WaitForNMI
  BIT PPUStatus
  LDA #$3F
  STA PPUAddr
  LDA #$00
  STA PPUAddr

  TSX
  STX SaveStack
  LDX #$FF
  TXS
  LDX #$07
.LoopPaletteToPPU:
  PLA
  STA PPUData
  PLA
  STA PPUData
  PLA
  STA PPUData
  PLA
  STA PPUData
  DEX
  BPL .LoopPaletteToPPU

  LDA #$20
  STA PPUAddr
  LDA #$60
  STA PPUAddr

  LDX #$07
.LoopPaletteToPPU2:
  PLA
  STA PPUData
  PLA
  STA PPUData
  PLA
  STA PPUData
  PLA
  STA PPUData
  DEX
  BPL .LoopPaletteToPPU2

  LDA #$20
  STA PPUAddr
  LDA #$A4
  STA PPUAddr
  LDX #$02 ;Number Of Loops

.LoopPaletteToPPU3:
  PLA
  STA PPUData
  PLA
  STA PPUData
  PLA
  STA PPUData
  PLA
  STA PPUData
  PLA
  STA PPUData
  PLA
  STA PPUData
  LDA PPUData
  LDA PPUData
  LDA PPUData
  DEX
  BPL .LoopPaletteToPPU3

  LDX SaveStack ;Retrieve stack back.
  TXS

  LDA GameStatus ;If test is started, show sprites.
  AND #%00010000
  EOR #%00001110
  STA PPUMaskRAM

  STA PPUMask ;Store to mask.
  LDA PPUCtrlRAM ;Set PPUCtrl
  STA PPUCtrl
  LDA #$00
  STA PPUScroll
  STA PPUScroll ;Reset scroll.
  STA PPUOamAddr ;Set OAM to 0.
  LDA #HIGH(SpritePage) ;Get sprites page.
  STA PPUDma ;Store to DMA to PPUOAM.

  JSR ReadControllers

  LDX #$03 ;Loop count.
  LDY #$02 ;Array index.
  LDA ControllersHigh
  ORA ControllersHigh+1 ;Grab both controllers bits.
.ControllerPaletteLoop:
  ASL A
  PHA ;Save A.
  BCC .GreyEntry1
  LDA PaletteRoll
  BNE .WriteEntry1
.GreyEntry1:
  LDA #$0F
.WriteEntry1:
  STA RAMPalette,Y
  INY
  PLA
  ASL A
  PHA ;Save A.
  BCC .GreyEntry2
  LDA PaletteRoll
  BNE .WriteEntry2
.GreyEntry2:
  LDA #$0F
.WriteEntry2:
  STA RAMPalette,Y
  PLA ;Get A again.
  INY
  INY
  INY
  DEX
  BPL .ControllerPaletteLoop

  JSR SpriteAnimationToScreen ;Put megaman to screen with his current step.

  LDA #%00010000
  BIT GameStatus
  BNE .GameStarted
  JSR WaitToStartGame
  JMP .AfterChooseEngine
.GameStarted:
  JSR MainGame
.AfterChooseEngine:

  JSR UpdateButtonsBuffer

  INC PRC
  LDA PRC
  AND #%00000011
  BNE .AfterPaletteInc
  INC PaletteRoll
  LDA PaletteRoll
  CMP #$2D
  BCC .AfterPaletteInc
  LDA #$21
  STA PaletteRoll
.AfterPaletteInc:
  JMP MainEngine ;Everything updated, jump back to wait for next VBlank to upload sprites and do it again.



NMI:
  INC Frame
IRQ:
  RTI


  .bank 1
  .org $E000


  .org $FFFA
  .dw NMI
  .dw Reset
  .dw IRQ


  .bank 2

  .incbin "Graphics/Graphics.chr"