WordsPage:
  .incbin "Screens/MainText.bin"
  .incbin "Screens/GameplayText.bin"
  .incbin "Screens/FinishedText.bin"

Pallet: .incbin "Graphics/MainPalette.pal"

MainScreen: .incbin "Screens/RLECompressedMainScreen.nam"

RLEPointersLow:
  .db LOW(MainScreen)

RLEPointersHigh:
  .db HIGH(MainScreen)

MegaManBase: ;(AnimationStep&#$03)*4=Number of tiles to add to base each time.
  .db $00,$05,$00,$00;YTAX Top left to top right.
  .db $00,$07,$00,$08;YTAX
  .db $00,$09,$00,$10;YTAX
  .db $00,$0B,$00,$18;YTAX

  .db $10,$25,$00,$00;YTAX Bottom right to bottom left.
  .db $10,$27,$00,$08;YTAX
  .db $10,$29,$00,$10;YTAX
  .db $10,$2B,$00,$18;YTAX

MegaManFace:
  .db $00,$03,$01,$00 ;These sprites all just go straight to OAM.

MegaManBaseXOffsets:
  .db $00,$F8,$F8,$F8

MegaManBaseFaceXOffsets:
  .db $0D,$15,$15,$15
MegaManBaseFaceYOffsets:
  .db $08,$06,$08,$06

BitTestHL:
  .db $80,$40,$20,$10,$08,$04,$02
BitTestLH:
  .db $01,$02,$04,$08,$10,$20,$40,$80

ButtonTexts:
  .db "AAAAAA"
  .db "BBBBBB"
  .db "SELECT"
  .db " START"
  .db "  UP  "
  .db " DOWN "
  .db " LEFT "
  .db " RIGHT"
  .db "------"
  .db "FINISH"


ABtn=0;
BBtn=1;
SelBtn=2;
StrBtn=3;
UpBtn=4;
DnBtn=5;
LtBtn=6;
RtBtn=7;

ButtonCombos: ;We have 44 presses to do.
  .db ((LtBtn<<4)+RtBtn),((LtBtn<<4)+RtBtn) ;1 LRLR
  .db ((UpBtn<<4)+DnBtn),((UpBtn<<4)+DnBtn) ;2 UDUD
  .db ((LtBtn<<4)+UpBtn),((LtBtn<<4)+UpBtn) ;3 LULU
  .db ((RtBtn<<4)+DnBtn),((RtBtn<<4)+DnBtn) ;4 RDRD
  .db ((StrBtn<<4)+SelBtn),((StrBtn<<4)+SelBtn) ;5 SsSs
  .db ((RtBtn<<4)+BBtn),((LtBtn<<4)+ABtn) ;6 RBLA
  .db ((BBtn<<4)+ABtn),((BBtn<<4)+ABtn) ;7 BABA
  .db ((UpBtn<<4)+RtBtn),((DnBtn<<4)+LtBtn) ; URDL
  .db ((RtBtn<<4)+RtBtn),((LtBtn<<4)+LtBtn) ;9 RRLL
  .db ((BBtn<<4)+BBtn),((ABtn<<4)+ABtn) ;10 BBAA
  .db ((StrBtn<<4)+StrBtn),((SelBtn<<4)+SelBtn) ;11 SSss
;  .db (Btn<<4+Btn),(Btn<<4+Btn) ;#
  

  