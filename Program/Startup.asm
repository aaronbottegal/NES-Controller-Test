  SEI          ; disable IRQs
  CLD          ; disable decimal mode
  LDX #$40
  STX $4017    ; disable APU frame IRQ
  LDX #$FF
  TXS          ; Set up stack
  INX          ; now X = 0
  STX $2000    ; disable NMI
  STX $2001  ; disable rendering
  STX $4010    ; disable DMC IRQs
  BIT PPUStatus
.StartupVBlankWait1:
  BIT PPUStatus
  BPL .StartupVBlankWait1
  LDA #$07
  STA <ZeroWrite
  LDY #$00
  STY <Zero
  LDA #$00
.ClearRAM: ;Also clears sprites.
  DEY
  STA [Zero],Y
  BNE .ClearRAM
  DEC <Zero+1
  BMI .StartupVBlankWait2
  LDX <ZeroWrite
  CPX #HIGH(SpritePage)
  BNE .ClearRAM-2
  LDA #$FF
  BNE .ClearRAM 
.StartupVBlankWait2:      ;Second wait for vblank, PPU is ready after this
  BIT PPUStatus
  BPL .StartupVBlankWait2